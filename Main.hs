{-# LANGUAGE OverloadedStrings #-}

module Main where

import Graphics.UI.Gtk hiding (Variant, Signal)
import Control.Concurrent
import Control.Monad

import DBus
import DBus.Client
import Data.Maybe

import Data.IORef
import Data.Word

import Control.Monad.Trans.Reader
import Control.Monad.Trans.Class

import Control.Applicative

import DBus.Internal.Types

data AppEnv = AppEnv { playerTitle    :: Label
                     , playerProgress :: ProgressBar
                     , playButton     :: Button
                     , pathRef        :: IORef [ObjectPath]
                     , prgRef         :: IORef (Word32, Word32)
                     , client         :: Client
                     }

type Controller m a = ReaderT AppEnv m a

str_ :: String -> String
str_ = id

dbusBluezDest :: Maybe BusName
dbusBluezDest = Just "org.bluez"

mediaPlayer1 :: String
mediaPlayer1 = "org.bluez.MediaPlayer1"

withDictionary :: Variant -> ([(Variant,Variant)] -> a) -> a
withDictionary v f = f . dictionaryItems . fromJust . fromVariant $ v

getConnectedPlayers :: [(Variant, Variant)] -> [ObjectPath]
getConnectedPlayers xs
  = [ fromJust . fromVariant $ k
    | (k, v) <- xs
    , isJust $ withDictionary v (lookup $ toVariant mediaPlayer1)
    ]

playerMethodCall :: ObjectPath -> MemberName -> MethodCall
playerMethodCall playerPath member =
  (methodCall playerPath (interfaceName_ mediaPlayer1) member)
  { methodCallDestination = dbusBluezDest
  }

callPlayerMethod :: Client -> IORef [ObjectPath] -> MemberName -> IO ()
callPlayerMethod client pathRef method = do
  paths <- readIORef pathRef
  when (not . null $ paths) $
    void $ call_ client $ playerMethodCall (head paths) method

getPlayerProperty :: Client -> IORef [ObjectPath] -> MemberName
                  -> IO (Maybe Variant)
getPlayerProperty client pathRef pp = do
  paths <- readIORef pathRef
  case paths of
    []      -> return Nothing
    path':_ -> do
      r <- getProperty client $ playerMethodCall path' pp
      case r of
        Left _    -> return Nothing
        Right pp' -> return $ Just pp'

handleSignal :: Signal -> Controller IO ()
handleSignal signal = processSignal sMember sPath sInterface sBody
  where sPath = signalPath signal
        sInterface = signalInterface signal
        sMember = signalMember signal
        sBody = signalBody signal

processSignal :: MemberName -> ObjectPath -> InterfaceName -> [Variant]
              -> Controller IO ()
processSignal "InterfacesAdded" "/" "org.freedesktop.DBus.ObjectManager"
              sBody = do
  pathRef <- asks pathRef
  paths <- lift $ readIORef pathRef
  when (isJust newPath &&
        isJust (withDictionary (sBody !! 1) (lookup $ toVariant mediaPlayer1))) $
    lift $ atomicModifyIORef pathRef $ \xs -> (xs ++ [fromJust newPath], ())
  where
    newPath = fromVariant . head $ sBody

processSignal "InterfacesRemoved" "/" "org.freedesktop.DBus.ObjectManager"
              sBody = do
  pathRef <- asks pathRef
  prgRef <- asks prgRef
  client <- asks client
  play <- asks playButton
  title <- asks playerTitle
  paths <- lift $ readIORef pathRef
  lift $ when ((not . null) paths && isJust thatPath) $ do
    when ((\v -> isJust v && mediaPlayer1 `elem` fromJust v)
          (fromVariant (sBody !! 1) :: Maybe [String])) $
      atomicModifyIORef pathRef $ \xs -> (filter (/= fromJust thatPath) xs, ())
    when (head paths == fromJust thatPath) $ do
      (tt, artist,
       duration, position, playText) <- getTrackAndPosition client pathRef
      postGUIAsync $ buttonSetLabel play playText
      let dr  = fromJust $ duration <|> pure 0
          pos = fromJust $ position <|> pure 0
      atomicWriteIORef prgRef (dr, pos)
      postGUIAsync $ do
        labelSetText title $ titleArtist tt artist
        widgetSetTooltipText title tt
  where
    thatPath = fromVariant . head $ sBody

processSignal "PropertiesChanged" sPath "org.freedesktop.DBus.Properties"
              sBody = do
  pathRef <- asks pathRef
  prgRef <- asks prgRef
  client <- asks client
  play <- asks playButton
  title <- asks playerTitle
  paths <- lift $ readIORef pathRef
  lift $ when ((not . null) paths && head paths == sPath &&
               (\v -> isJust v && fromJust v == mediaPlayer1)
               (fromVariant $ head sBody)) $ do
    let (k, v) = withDictionary (sBody !! 1) head
    case fromVariant k :: Maybe String of
          Just "Status" -> do
            playText <- buttonGetLabel play :: IO String
            let newPlayText = case extractFromVariant v :: Maybe String of
                                Just "playing" -> str_ "Pause"
                                _              -> str_ "Play"
            -- putStrLn $ "playText: " ++ playText
            --   ++ " newPlayText: " ++ newPlayText
            newPlayText' <-
              if playText == newPlayText
              then do
                threadDelay 1000000
                status <- getPlayerProperty client pathRef "Status"
                return $ case status >>= fromVariant :: Maybe String of
                           Just "playing" -> str_ "Pause"
                           _              -> str_ "Play"
              else return newPlayText
            -- putStrLn $ "newPlayText': " ++ newPlayText'
            postGUIAsync $ buttonSetLabel play newPlayText'
          Just "Track" -> do
            let (tt, ar, dr) = getInfoFromTrack (fromVariant v)
            when (isJust tt) $ postGUIAsync $ do
              labelSetText title $ titleArtist tt ar
              widgetSetTooltipText title tt
            when (isJust dr) (atomicModifyIORef prgRef
                               (\(_, p) -> ((fromJust dr, p), ())))
          Just "Position" ->
            let vv = extractFromVariant v
            in when (isJust vv) $
                 atomicModifyIORef prgRef $ \(d, _) -> ((d, fromJust vv), ())
          _ -> return ()

processSignal _ _ _ _ = return ()

getInfoFromTrack :: Maybe Variant
                 -> (Maybe String, Maybe String, Maybe Word32)
getInfoFromTrack track =
  let Just result =
        (track >>= (`withDictionary` \d ->  pure ( lookup' "Title" d
                                                 , lookup' "Artist" d
                                                 , lookup' "Duration" d
                                                 )))
        <|> pure (Nothing, Nothing, Nothing)
  in result
  where
    lookup' :: IsValue a => String -> [(Variant,Variant)] -> Maybe a
    lookup' item dict = lookup (toVariant item) dict
                        >>= extractFromVariant

updateProgressBar :: Controller IO ()
updateProgressBar = do
  prgRef <- asks prgRef
  play <- asks playButton
  progress <- asks playerProgress
  lift $ forever $ do
    (dr, pos) <- readIORef prgRef
    playText <- buttonGetLabel play
    postGUIAsync $ progressBarSetFraction progress $
      if (dr == 0) then 0 else fromIntegral pos / fromIntegral dr
    when (playText == str_ "Pause") $
      atomicModifyIORef prgRef (\(dr', pos') -> ((dr', pos'+1000), ()))
    threadDelay 1000000

getTrackAndPosition :: Client -> IORef [ObjectPath]
                    -> IO (Maybe String, Maybe String,
                           Maybe Word32, Maybe Word32, String)
getTrackAndPosition client pathRef = do
  track <- getPlayerProperty client pathRef "Track"
  status <- getPlayerProperty client pathRef "Status"
  pos <- getPlayerProperty client pathRef "Position"
  let (title, artist, duration) = getInfoFromTrack track
      position = pos >>= fromVariant
      playText = case status >>= fromVariant :: Maybe String of
                   Just "playing" -> "Pause"
                   _              -> "Play"
  return (title, artist, duration, position, playText)

titleArtist :: Maybe String -> Maybe String -> String
titleArtist title artist = fromJust $
  (++) <$> title <*> (('\n':) <$> artist <|> pure "") <|> pure ""

main :: IO ()
main = do
  client <- connectSystem

  reply1 <- call_ client (methodCall "/org/freedesktop/DBus"
                          "org.freedesktop.DBus"
                          "GetNameOwner")
            { methodCallBody = [toVariant $ str_ "org.bluez"]
            , methodCallDestination = Just "org.freedesktop.DBus"
            }
  reply2 <- call_ client (methodCall "/"
                          "org.freedesktop.DBus.ObjectManager"
                          "GetManagedObjects")
            { methodCallDestination = dbusBluezDest
            }

  let Just dbusBluezUniqName = fromVariant . head $ methodReturnBody reply1
      Just v = fromVariant . head $ methodReturnBody reply2
      playerPaths = getConnectedPlayers $ dictionaryItems v

  pathRef <- newIORef playerPaths
  (title, artist,
   duration, position, playText) <- getTrackAndPosition client pathRef
  prgRef <- newIORef (fromJust $ duration <|> pure 0,
                      fromJust $ position <|> pure 0)

  initGUI
  window <- windowNew
  on window objectDestroy mainQuit
  set window [ containerBorderWidth := 10
             , windowTitle := str_ "bluetooth-player-controller"
             , windowDefaultWidth := 400 ]
  vbox <- vBoxNew False 10
  label <- labelNew $ Just $ titleArtist title artist
  set label [ labelJustify := JustifyCenter
            , labelEllipsize := EllipsizeEnd
            , widgetTooltipText := title ]
  progress <- progressBarNew
  hbuttonbox <- hButtonBoxNew
  set vbox [ containerChild := label
           , containerChild := progress
           , containerChild := hbuttonbox ]
  set window [ containerChild := vbox ]
  prev <- buttonNewWithLabel $ str_ "Prev"
  play <- buttonNewWithLabel $ str_ playText
  next <- buttonNewWithLabel $ str_ "Next"
  on prev buttonActivated $ callPlayerMethod client pathRef "Previous"
  on next buttonActivated $ callPlayerMethod client pathRef "Next"
  on play buttonActivated $ do
    playText <- buttonGetLabel play
    callPlayerMethod client pathRef $ memberName_ playText
  set hbuttonbox [ containerChild := button
                 | button <- [prev, play, next] ]
  set hbuttonbox [ buttonBoxLayoutStyle := ButtonboxSpread ]
  widgetShowAll window

  let appEnv = AppEnv { playerTitle = label
                      , playerProgress = progress
                      , playButton = play
                      , pathRef = pathRef
                      , prgRef = prgRef
                      , client = client
                      }

  addMatch client matchAny { matchSender = Just dbusBluezUniqName
                           } $ \s -> runReaderT (handleSignal s) appEnv
  forkIO $ runReaderT updateProgressBar appEnv

  mainGUI
